/*
Filename: index.js
Author: Charles P. Cross
Description: Elevator API - Application initiation file.
*/

const express = require('express');
const app = express();
const swaggerUi = require('swagger-ui-express');
const swaggerDoc = require('./swagger.json');

global.lastElevatorId = 0;
global.lastBuildingId = 0;
global.lastQueueEntryId = 0;
global.validationError = "";

let db = require(`./models/database`);
let Building = require(`./modules/building`);
let Elevator = require(`./modules/elevator`);
let QueueEntry = require(`./modules/queue-entry`);
let ElevatorController = require(`./controllers/elevator-controller`);
let BuildingController = require(`./controllers/building-controller`);
let AppController = require(`./controllers/app-controller`);

const buildingsRouter = require(`./api-routes/buildings`);
const elevatorsRouter = require(`./api-routes/elevators`);
const homeRouter = require(`./api-routes/home`);

// Temp Local storage
global.buildings = [
    new Building({id:1, name:'Empire State Building',floors:102,elevatorsAvailable:57}),
    new Building({id:2, name:'Willis Tower',floors:108,elevatorsAvailable:104}),
    new Building({id:3, name:'Burj Khalifa',floors:163,elevatorsAvailable:57}),
    new Building({id:4, name:'Small Office Bulding',floors:16,elevatorsAvailable:4})
];

app.use('/docs', swaggerUi.serve, swaggerUi.setup(swaggerDoc));
app.use('/buildings', [elevatorsRouter, buildingsRouter]);
app.use('/', homeRouter);



// Initialize Server - if PORT is set in server environment then use that port else default to 3000
const port = process.env.PORT || 3000;
app.listen(port, () => console.log(`Listening on port ${port}...`));
