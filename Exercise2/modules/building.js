// Filename: building.js
// Description: Implementaiton of a building object. Buildings can have many elevators. Zero to many elevators in a building.
// Author: Charles P. Cross
// Zero to many elevators in a building. 
// Buildings can have many elevators ( dynamic number of elevators) in this relationship.

let Elevator = require(`./elevator.js`);
let QueueEntry = require(`./queue-entry`);
let AppController = require(`../controllers/app-controller`);

console.log("Building Class Initialized");

function Building(params) {

    this.id = ++lastBuildingId;
    this.name = params.name;
    this.floors = params.floors;
    this.elevatorsAvailable = params.elevatorsAvailable; 
    this.elevators = [];
    this.actionQueue = [];
    for (var i = 0; i < params.elevatorsAvailable; i++) {    	
        this.elevators.push(new Elevator({
	            buildingId: this.id,
	            availableFloors: this.floors
	            }
        ));
    }
}

Building.prototype.addToActionQueue = function(queueEntry){	
	this.actionQueue.push(queueEntry);
}

Building.prototype.startActionQueue = function(){
	// Only process queue if there is a queue for this building.
	if(this.actionQueue.length > 0){
		console.log(''); // Space in console log output
		console.log('//////////////////////////////////////////////////////////////'); // Divider
		console.log(`[Building id: ${this.id} Action Queue to Process]:`);	
		console.log(''); // Space in console log output	
		console.log(`${JSON.stringify(this.actionQueue)}`);
		let actionQueue = this.actionQueue;
		let elevators = this.elevators;
		let eventCount = 0;
		//let elevatorToUpdate;
		let buildingIdForQueue = this.id; // Building id for this event action queue.
		this.actionQueue.forEach(function(queueEntry) {
		    eventCount++;		    
	    	console.log(''); // Space in console log output
	    	console.log('//////////////////////////////////////////////////////////////'); // Divider
	        console.log(`ACTION EVENT #: ${eventCount}`); 
	        console.log('//////////////////////////////////////////////////////////////'); // Divider
	        console.log(''); // Space in console log output	 	        
		    console.log("[Event QueueEntry to Process]: " + JSON.stringify(queueEntry));
		    console.log(''); // Space in console log output	 
	    	// Apply action to elevator
	    	let elevator = elevators.find(e => e.id === queueEntry.elevatorId);

	    	queueEntry.elevatorBeforeState = JSON.parse(JSON.stringify(elevator));
	    	console.log("[ELEVATOR_STATE Before Action]: " + JSON.stringify(elevator));
		    if(queueEntry.action == "goToFloor"){
		    	elevator.doorOpen = false;
		  	    elevator.updateStatus("Moving");
	    		elevator.moveToNextFloor();
	    		elevator.updateStatus("Arrived");
	    		elevator.doorOpen = true;
	    	}
	    	if(queueEntry.action == "openDoor"){
	    		elevator.updateStatus("OpeningDoor");
	    		elevator.openDoor();
	    		elevator.updateStatus("DoorOpen");
	    	}
	    	if(queueEntry.action == "closeDoor"){
	    		elevator.updateStatus("ClosingDoor");
	    		elevator.closeDoor();
	    		elevator.updateStatus("DoorClosed");   		
	    	}
	    	console.log("[ELEVATOR_STATE After Action]: " + JSON.stringify(elevator));
	    	queueEntry.elevatorAfterState = JSON.parse(JSON.stringify(elevator));
	    	elevator.updateStatus("Waiting");
	    	//elevatorToUpdate = elevator; // So that we can access this elevator object ouside of the above scope.    
		    
		});
		
		console.log('');
		console.log(`[[BUILDING QUEUE COMPLETE] for building id: ${buildingIdForQueue}`);			
		console.log('');
	    // Queue is done. Go to waiting Position.
	    /*
	    console.log("[ElevatorState BeforeAction]: " + JSON.stringify(elevatorToUpdate));
		elevatorToUpdate.updateStatus("MovingToWaitPosition");
	    elevatorToUpdate.waitInPosition();
	    elevatorToUpdate.updateStatus("Arrived");
	    elevatorToUpdate.updateStatus("Waiting");
	    console.log("[ELEVATOR_STATE AfterAction]: " + JSON.stringify(elevatorToUpdate));
	    */
	    console.log('');
		console.log(`[!*Elevators Finished*!] for building id: ${buildingIdForQueue}`);	
	    console.log('//////////////////////////////////////////////////////////////'); // Divider		

		// Empty actionQueue.
		this.actionQueue= [];		
	}
}

module.exports = Building;