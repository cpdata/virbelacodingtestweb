// Filename: elevator.js
// Description: 
// Author: Charles P. Cross
let AppController = require(`../controllers/app-controller`);

console.log("Elevator Class Initialized");

function Elevator(params) {
    
    this.id = ++lastElevatorId;
    this.buildingId = params.buildingId; 
    this.status = "Waiting";
    this.currentFloor = 1;
    this.availableFloors = params.availableFloors;
    this.doorOpen = false;
    this.lastDirection = "down";
    this.elevatorOdometer = 0;
    this.stopQueue = [];
}

Elevator.prototype.openDoor = function(){                
    // ... Hypothetically wait for passengers ...
    this.doorOpen = true;
    console.log(`Action: 'OpenDoor' Elevator: ${JSON.stringify(this)}`);                
}

 
Elevator.prototype.closeDoor = function() {
    // Change doorOpen value. When door is closed. New floors can be scheduled and added to the "stopQueue".
    this.doorOpen = false;
    console.log(`Action: 'CloseDoor' Elevator: ${JSON.stringify(this)}`);
            
}

Elevator.prototype.goToFloor = function(floorNum){
    // Add a "floorNum" to the elevator's "stopQueue" to schedule the elevator to stop on that floor number asap.
	console.log(`Added to Building Queue - Action: 'goToFloor' FloorNum: ${floorNum} Elevator id: ${this.id} Building id: ${this.buildingId}`);
    this.stopQueue.push(parseInt(floorNum));
    this.optimizeStopQueue();    
}

Elevator.prototype.optimizeStopQueue = function(){
    // Re-Order stopQueue for effeciency yet insure all floor stops get completed in reasonable time.

    // Log Action once function is written.
	//console.log(`Action: 'optimizeStopQueue' Elevator id: ${this.id} Building id: ${this.buildingId}`);
    
    // PHASE 3...
    // Add some sort of logic to have the elevator intelligently position itself when not actively being used.
    // (e.g. if it was an office building, idle on lower floors in morning, upper or middle floors at the end of the workday)
}

Elevator.prototype.optimizeWaitPosition = function(){
    // This could be much more advanced. Moving to center for simplicity sake. 
    return Math.round((this.availableFloors / 2)); // Wait in the center of building.    
}
Elevator.prototype.waitInPosition = function(){
    // Position Elevator where it is most likely to have shortest distance for it's next trip.
	console.log(`Action: 'waitInPosition' Elevator id: ${this.id} Building id: ${this.buildingId}`);
    //determine best position to wait.    
    let nextFloor = this.optimizeWaitPosition();
    //now go to wait position
    this.status = "MovingToWaitPosition";
    this.moveToFloor(nextFloor);        
}

Elevator.prototype.moveToNextFloor = function(){
    // Moves elevator to next floor in it's stopQueue
	//console.log(`Action: 'moveToNextFloor' Elevator id: ${this.id} Building id: ${this.buildingId}`); 
	this.moveToFloor(this.stopQueue[0]);
}

Elevator.prototype.moveToFloor = function(floorNum){
    // Move Elevator to a new floor.
    let startFloor = this.currentFloor;
    let distanceFromLastStop = 0;

    if(this.currentFloor > floorNum){
        // Going Down...
        distanceFromLastStop = this.currentFloor - floorNum;
        this.currentFloor = floorNum;
        this.lastDirection = "down";
    }

    if(this.currentFloor < floorNum){
        // Going Up..
        distanceFromLastStop = floorNum - this.currentFloor;
        this.currentFloor = floorNum;
        this.lastDirection = "up";
    }    

    // Track how far we are along the route.
    this.elevatorOdometer += distanceFromLastStop;
    let stopFloor = this.currentFloor;
    console.log('');
    console.log(`ELEVATOR MOVING: From FloorNum: ${startFloor} ${this.lastDirection} to FloorNum ${stopFloor}`);
    
    // Remove current floor from stopQueue.
    this.stopQueue = this.stopQueue.filter((floor) =>{
        return floor != this.currentFloor;
    });
}

Elevator.prototype.startQueue = function(){
	console.log(`Action: 'startQueue' Elevator id: ${this.id} Building id: ${this.buildingId}`);
    if(this.stopQueue.length > 0){            
        this.moveToFloor(this.stopQueue[0]);
    }
}

Elevator.prototype.updateStatus = function(status){                    
    this.status = status;
    console.log(''); // Space in console log output 
    console.log(`Elevator Status Update: '${this.status}' Elevator id: ${this.id} Building id: ${this.buildingId}`);
    console.log(''); // Space in console log output 
}

module.exports = Elevator;