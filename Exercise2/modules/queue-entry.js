// Filename: queue-entry.js
// Description: 
// Author: Charles P. Cross

let AppController = require(`../controllers/app-controller`);

function QueueEntry(params) {
	lastQueueEntryId++;
    this.id = lastQueueEntryId;
    this.action = params.action;
    this.buildingId = params.buildingId;
    this.elevatorId = params.elevatorId;
    if(params.floorNum > 0){
        this.floorNum = params.floorNum;    	
    }
    this.timestamp = AppController.getDateTime();
}

QueueEntry.prototype.refresh = function(actionQueue){
	lastQueueEntryId = 0;
	actionQueue = [];
}

module.exports = QueueEntry;