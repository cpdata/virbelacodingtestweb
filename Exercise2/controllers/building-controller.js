// Filename: database.js
// Author: Charles P. Cross
// Description: Controls access to the database.
// Filename: building-controller.js
// Author: Charles P. Cross
// Description: Controls adding the buildings.
let db = require(`../models/database`);
let Building = require(`../modules/building`);
let AppController = require(`../controllers/app-controller`);

class BuildingController{

    // Post a new building to the database
    static addNewBuilding(req){
	    // Validation for adding a new Building
	    if(!(isNaN(req.query.name) && req.query.name != "")){
	        // New Building does not meet Validation Requirements.
	        validationError += "Invalid Building Parameter:[name] Name cannot be a number or empty string. Must include letters. ";        
	        return false;
	    }
	    if(!(parseInt(req.query.floors) > 0)){
	        // New Building does not meet Validation Requirements.
	        validationError += "Invalid Building Parameter:[floors] Building must have greater than 0 floors. Must be a numerical integer. ";
	        return false;
	    }
	    if(!(parseInt(req.query.elevators) > 0)){
	        // New Building does not meet Validation Requirements.
	        validationError += "Invalid Building Parameter:[elevators] Building must have greater than 0 elevators. Must be a numerical integer. ";
	        return false;
	    }
	    req.query.elevatorsAvailable = req.query.elevators;    
	    let building = new Building(req.query);

	    // Passed Validation. Push to buildings object.    
	    buildings.push(building);
	    // Now lets save the update to the database.
	    if(!db.update("buildings",buildings)){
	        validationError += "Building creation failed.";        
	        return false;
	    }
	    // Data is correct and building successfully updated. If we made it this far we can return true.
	    return true;
    }
}

module.exports = BuildingController;