// Filename: elevator-controller.js
// Author: Charles P. Cross
// Description: Controls adding the elevators.
let db = require(`../models/database`);
let Elevator = require(`../modules/elevator`);
let AppController = require(`../controllers/app-controller`);

class ElevatorContoller{

    // Post a new elevator to the database
    static addNewElevator(req){    
	    let building = buildings.find(b => b.id === parseInt(req.params.id));
	    // Check if building elevators object could be found. 
	    if(!building.elevators){
	        validationError += "Could not find building with given ID. ";
	        return false;
	    }
	    // Add new elevator to elevator list object for a specific building.    
	    building.elevators.push(new Elevator({        
	            buildingId: parseInt(req.params.id), 
	            status:"Waiting", 
	            currentFloor: 1, 
	            availableFloors: building.floors, 
	            doorOpen: false, 
	            lastDirection:"down", 
	            stopQueue:[]
	        }
	    ));    
	    // Now lets save the update to the database.
	    if(!db.update("elevators",building.elevators)){
	        validationError += "Could not save new elevator to database. ";
	        return false;
	    }
	    // Data is correct and elevator successfully updated. If we made it this far we can return true.
	    return true;
	}
}

module.exports = ElevatorContoller;