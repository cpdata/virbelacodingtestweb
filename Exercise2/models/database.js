// Filename: database.js
// Author: Charles P. Cross
// Description: Controls access to the database.
let AppController = require(`../controllers/app-controller`);

class Database{

    static update (resourceName, dataObject){	

		if(resourceName === "buildings"){

	        return true;
	    }
	    if(resourceName === "building"){

	        return true;
	    }
	    if(resourceName === "elevators"){

	        return true;
	    }  
	    if(resourceName === "elevator"){

	        return true;
	    }
	    // The resourceName was not provided. So database update fails.
	    return false;
	}
}

module.exports = Database;