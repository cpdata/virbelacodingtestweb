# Exercise 2 #

For this exercise you will create a REST API that supports data retrieval and requests to support elevator operation.

As you progress through the steps, feel free to add comments to the code about *why* you choose to do things a certain way. Add comments if you felt like there's a better, but more time intensive way to implement specific functionality. It's OK to be more verbose in your comments than typical, to give us a better idea of your thoughts when writing the code.

### What you need ###

* IDE of your choice
* Git
* Some chosen backend language / framework
* Some chosen local data store

## Instructions ##

### Phase 1 - Setup ###

 1. Clone this repository to your local machine
 1. Create the basic structure needed for your API with your chosen framework
 1. Add a README.md in this exercise folder with the basic requirements and steps to run the project locally

### Phase 2 - Main Implementation ###

Implement a RESTful API to support zero to many elevators in a building. Buildings *have many* elevators in this relationship. Must meet the following requirements:

 * API call can request information about the building, which will return all available elevators
 * API call can request information about an elevator in a building including, but not limited to: id, status, current floor, available floors
 * API call can command an elevator to perform actions including, but not limited to: open door, close door, go to floor

 Notes on elevator state:
 - For this phase we assume that any elevator actions are instantaneous
 - "go to floor" request for an elevator results in instant arrival at the floor and opening of the door

### Phase 3 - Stretch Goals ###

Please implement any of the following stretch goals. They are in no particular order.

 * Unit tests
 * Add some sort of logic to have the elevator intelligently position itself when not actively being used (e.g. if it was an office building, idle on lower floors in morning, upper or middle floors at the end of the workday)
 	* This could be configurable or "smart" based on trends
 * Add some type of self-documenting UI such as Swagger

### Phase 4 - SUPER STRETCH GOAL - Add queuing of floor stops ###

* Elevator actions are no longer instantaneous
	* Actions are now queued and all actions will execute when a separate API request "go" is called
	* "Go" should be applied to all elevators in a building, and all queued actions execute instantaneously
	* Elevator actions should be logged (console output is fine) and the final state available immediately

Notes:
- The concept of requesting to go up or down can be ignored for simplicity. Just stick with "go to floor"
- The elevator should be efficient in stopping at floors. If the elevator is on floor 1 and "go to floor" is queued for 5 different floors, the elevator should stop at each queued floor in a logical, efficient sequence.

## Questions ##

 1. How can your implementation be optimized?
 1. How much time did you spend on your implementation?
 1. What was most challenging for you?

## Next Steps ##

* Confirm you've addressed the functional goals
* Answer the questions above by adding them to this file
* Make sure your README.md is up to date with setup and run instructions
* Ensure you've followed the sharing instructions in the main [README](../README.md)

# Awnsers to Exercise Questions

1. How can your implementation be optimized?
- A: In a production environment it would be optimal if the building and elevator state data was stored in an AWS DynamoDB for easy scalable storage. Utilizing AWS's SQS (Simple Queue Service) would allow many servers/instances/workers to process the action queues for the buildings. To further optimize the production environment I would run the RESTful API on AWS lambda functions. By using those AWS services in conjuction with AWS's API gateway the implementation would be a No-Server or Serverless RESTful API which would be effecient for development, security, and maintainence. This implementation being a Serverless deployment would make it optimal to scale infinitly and linearly and reducing costs to only pay for the exact amount of compute resources utilized and remove the need for complex server administration while also being a more secure production deployment solution.

How I would optimize the implementation of the design of the code for this exersice? I would add PUT/UPDATE and DELETE routes for the Buildings and Elevators to expand the implemetned CREATE & READ actions to have complete CRUD for the resource collections. I would implement a proper database such as mongoDB or AWS's DynamoDB for storing the state information of the elevators and buildings.

Passenger logic could be added with a passenger REST routes and js objects to represent them. Passenget objects could play into the dynamics of how elevators in a system actually get used which would create a more complete system to model elevators in a building. They would be factored into capacity limits of both the buildings and elevators which would add more realistic complexity to the triggering of actions and additional stops for overcapacity stops. Knowing average capacity for elevators for times of day and on which floors would enable more intellegent elevator wait positioning.

For the logic of the elevators I would add values for capacity and speed/rate of floors per minute/sec. Then add delays for opening and closing the doors. The sequence of actions could be spread out and delayed to simulate a real elevator. Furthermore adding "up" and "down" values to the goToFloor api call to simulate how real elevators work. By having that additional logic I would be able to make a true to world algorithm for how the elevator sequence between floors picks up passengers from each floor. 

For the logic of the buildings I would add floor capacity limits as well as current and average capacity. This would allow more intellgent goToWaitPosition logic to position the elevator in positions that are most likely to reduce the distance needed to travel to stop at the next floor for passenger pickup.

To further optimize or enhance this project a client side front-end to go with it would help visualize the actions of the RESTful api. 

I think with all of the above optimizations this project could be a full elevator simulator. The algorithm to optimize the sequence between the floors could be set with controlable parameters to make a game out with the aim being the elevator doorman that is trying to move as many passengers in the least amount of time/distance. Basically being a elevator doorman time trial game. 

Security - Add apikey and/or auth credentials

2. How much time did you spend on your implementation?
- A: 

- Plan & Design: 30 Min
- Phase 1 Setup: 30 Min 
- Setup DEMO on web facing Server w/ SSL: 30 Min
- Write Code: 
   - RESTful API Phase 2: 4 hrs    
   - Queue Phase 4: 1.5 hr
- Create Postman Collection & Testing: 1 hr

Total: 8 hours

3. What was most challenging for you?
- A: Showcasing my expertise as much as possible within a small scope using the requirements of this exercise.

