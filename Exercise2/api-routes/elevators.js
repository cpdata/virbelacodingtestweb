// Filename: elevators.js
// Description: Handles the elevators collection for each building
// Author: Charles P. Cross

const express = require('express');
const router = express();
let db = require(`../models/database`);
let Building = require(`../modules/building`);
let Elevator = require(`../modules/elevator`);
let QueueEntry = require(`../modules/queue-entry`);
let ElevatorController = require(`../controllers/elevator-controller`);
let AppController = require(`../controllers/app-controller`);

// getElevators
// LIST - Get the elevators list object which contains all of the elevator objects for a building.
// METHOD: GET
router.get("/:id/elevators", (req, res) => {
    const building = buildings.find(b => b.id === parseInt(req.params.id));    
    if(!buildings){
        // Return HTTP Code: 404 (Resource Not Found)
        res.status(501).send('There are no buldings!');
        return;
    }
    if(!building){
        // Return HTTP Code: 404 (Resource Not Found)
        res.status(404).send(`The building with the given ID does not exist. ${req.params.id}`);
        return;
    }
    // Check elevators are accessible and exist
    if(!building.elevators){
        res.status(501).send('Opps.. Something went wrong. The elevators are currently inaccessible. Try again later.');
        return;
    }
    res.status(200).send(building.elevators);
    return;
});

// openDoor
// Will openDoor
// METHOD: GET
router.get("/:id/elevators/:elevatorId/open-door", (req, res) => {      
    const building = buildings.find(b => b.id === parseInt(req.params.id));
    if(!building){
        // Return HTTP Code: 404 (Resource Not Found)
        res.status(404).send('The building with the given ID does not exist.');
        return;
    }
    // Search for elevator object using the id provided in the request.
    const elevator = building.elevators.find(e => e.id === parseInt(req.params.elevatorId));
    // Check if elevator object could be found. 
    if(!elevator){
        // Return HTTP Code: 404 (Resource Not Found)
        res.status(404).send('The elevator with the given parameters does not exist.');
        return;
    }    
    // Put Action in Queue       
    building.addToActionQueue(new QueueEntry({action:'openDoor', buildingId: parseInt(building.id), elevatorId: parseInt(elevator.id)}));    
    console.log(`Added to Building Queue - Action: 'openDoor' start queue for Building id: ${building.id}`);    
    // Return elevator object for the ID provided in the request.
    res.status(200).send(building.actionQueue);
    return;
});

// closeDoor
// Will closeDoor
// METHOD: GET
router.get("/:id/elevators/:elevatorId/close-door", (req, res) => {      
    const building = buildings.find(b => b.id === parseInt(req.params.id));
    if(!building){
        // Return HTTP Code: 404 (Resource Not Found)
        res.status(404).send('The building with the given ID does not exist.');
        return;
    }
    // Search for elevator object using the id provided in the request.
    const elevator = building.elevators.find(e => e.id === parseInt(req.params.elevatorId));
    // Check if elevator object could be found. 
    if(!elevator){
        // Return HTTP Code: 404 (Resource Not Found)
        res.status(404).send('The elevator with the given parameters does not exist.');
        return;
    }
    // Put Action in Queue    
    building.addToActionQueue(new QueueEntry({action:'closeDoor', buildingId: parseInt(building.id), elevatorId: parseInt(elevator.id)}));
    console.log(`Added to Building Queue - Action: 'closeDoor' start queue for Building id: ${building.id}`);    
    // Return elevator object for the ID provided in the request.
    res.status(200).send(building.actionQueue);
    return;
});

// goToFloor
// Will goToFloor when "elevator.startQueue" is called.
// METHOD: GET
router.get("/:id/elevators/:elevatorId/go-to-floor/:floorNum", (req, res) => {      
    let building = buildings.find(b => b.id === parseInt(req.params.id));    
    if(!building){
        // Return HTTP Code: 404 (Resource Not Found)
        res.status(404).send('The building with the given ID does not exist.');
        return;
     
    }
    // Search for elevator object using the id provided in the request.
    let elevator = building.elevators.find(e => e.id === parseInt(req.params.elevatorId));
    // Check if elevator object could be found. 
    if(!elevator){
        // Return HTTP Code: 404 (Resource Not Found)
        res.status(404).send('The elevator with the given parameters does not exist.');
        return;
    }
    if(isNaN(req.params.floorNum) || req.params.floorNum <= 0){
        res.status(400).send('Bad Request. The elevator cannot go to invalid floor number.'); 
        return;
    }
    // Bad Request if trying to go to a floor that doesn't exist for the building the elevator is in.
    if(req.params.floorNum > elevator.availableFloors ){
        res.status(400).send(`Bad Request. The elevator cannot go to floor ${req.params.floorNum}. ${building.name} has only ${parseInt(building.floors)} floors.`);
        return;
    }     
    // Add the floor to the stop queue and wait until "startQueue" is called.
    elevator.goToFloor(req.params.floorNum);
    // Put Action in Queue       
    building.addToActionQueue(new QueueEntry({action:'goToFloor', buildingId: parseInt(building.id), elevatorId: parseInt(elevator.id), floorNum:parseInt(req.params.floorNum)}));    
    // Return elevator object for the ID provided in the request.
    res.status(200).send(building.actionQueue);
    return;
});

// addElevator
// CREATE - Add a new elevator to a building.
// METHOD: POST
// ! idempotent : false
router.post("/:id/elevators", (req, res) => {
    let building = buildings.find(b => b.id === parseInt(req.params.id));
    if(!building){
        // Return HTTP Code: 404 (Resource Not Found)
        res.status(404).send('The building with the given ID does not exist.');
        return;
    }
    // Check elevators are accessible and exist
    if(!building.elevators){
        //customHttpResponseJson(501,res);
        res.status(500).send('Opps.. Something went wrong. The elevators are currently inaccessible. Try again later.');
        return;
    }
    // Attempt to add a new elevator and validate the posted variables. Otherwise reject request.
    if(!ElevatorController.addNewElevator(req)){        
        res.status(400).send(`Error: New elevator does not meet Validation Requirements. ${validationError} `);
        return;
    }
    // Update building with a new elevator object
    building = buildings.find(b => b.id === parseInt(req.params.id));
    // Adding new elevator was successful now lets return the newley created elevator object.        
    res.status(200).send(building.elevators);
    return;
});

// deleteElevator
// READ - Delete elevator object. 
// METHOD: GET
router.delete("/:id/elevators/:elevatorId", (req, res) => {          
    // Search for building object using the id provided in the request.
    const building = buildings.find(b => b.id === parseInt(req.params.id));
    // Check if building object could be found. 
    if(!building){
        // Return HTTP Code: 404 (Resource Not Found)
        res.status(404).send('The building with the given ID does not exist.');
        return;
    }
    // Search for elevator object using the id provided in the request.
    const elevator = building.elevators.find(e => e.id === parseInt(req.params.elevatorId));
    // Check if elevator object could be found. 
    if(!elevator){
        // Return HTTP Code: 404 (Resource Not Found)
        res.status(404).send('The elevator with the given parameters does not exist.');
        return;
    }
    building.elevators = building.elevators.filter((keepElevator) =>{
        return keepElevator != elevator;
    });
    // Return building object for the ID provided in the request.
    res.status(200).send(building);
    return;
});

// findElevators
// READ - Search for a elevator object. Uses the building "id" and "elevatorId" parameters to return a specific elevator object for the response body.
// METHOD: GET
router.get("/:id/elevators/:elevatorId", (req, res) => {      
    const building = buildings.find(b => b.id === parseInt(req.params.id));
    if(!building){
        // Return HTTP Code: 404 (Resource Not Found)
        res.status(404).send('The building with the given ID does not exist.');
        return;
    }
    // Search for elevator object using the id provided in the request.
    const elevator = building.elevators.find(e => e.id === parseInt(req.params.elevatorId));
    // Check if elevator object could be found. 
    if(!elevator){
        // Return HTTP Code: 404 (Resource Not Found)
        res.status(404).send('The elevator with the given parameters does not exist.');
        return;
    }
    // Return elevator object for the ID provided in the request.
    res.status(200).send(elevator);
    return;
});

module.exports = router;