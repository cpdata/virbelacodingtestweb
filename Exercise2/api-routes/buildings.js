// Filename: buildings.js
// Description: Handles the buildings collection containing each building.
// Author: Charles P. Cross

const express = require('express');
const router = express.Router();
let db = require(`../models/database`);
let Building = require(`../modules/building`);
let Elevator = require(`../modules/elevator`);
let QueueEntry = require(`../modules/queue-entry`);
let BuildingController = require(`../controllers/building-controller`);
let AppController = require(`../controllers/app-controller`);

// getBuildings
// LIST - Get the buildings resource object which contains all the buildings & elevators
// METHOD: GET
router.get("/", (req, res) => {
    // Check buildings are accessible and exist
    if(!buildings){        
        res.status(501).send('The buildings are currently inaccessible. Try again later.');
        return;
    }
    res.status(200).send(buildings);
    return;
});

// addBuilding
// CREATE - Add a new building to the buildings resource object
// METHOD: POST
// ! idempotent : false
router.post("/", (req, res) => {
    // Check buildings are accessible and exist 
    if(!buildings){        
        res.status(501).send('The buildings are currently inaccessible. Try again later.');
        return;
    }
    // Attempt to build add a new building and validate the posted variables. Otherwise reject request.
    if(!BuildingController.addNewBuilding(req)){
        console.log(`Error: ${validationError}`);
        res.status(400).send(`Error: ${validationError}`); //"New building does not meet Validation Requirements."
        return;
    }
    res.status(200).send(buildings);
    return;
});

// findBuilding
// READ - Search for a building object.
// METHOD: GET
router.get("/:id", (req, res) => {          
    // Search for building object using the id provided in the request.
    const building = buildings.find(b => b.id === parseInt(req.params.id));
    // Check if building object could be found. 
    if(!building){
        // Return HTTP Code: 404 (Resource Not Found)
        res.status(404).send('The building with the given ID does not exist.');
        return;
    }
    // Return building object for the ID provided in the request.
    res.status(200).send(building);
    return;
});

// deleteBuilding
// READ - Delete building object. 
// METHOD: GET
router.delete("/:id", (req, res) => {          
    // Search for building object using the id provided in the request.
    const building = buildings.find(b => b.id === parseInt(req.params.id));
    // Check if building object could be found. 
    if(!building){
        // Return HTTP Code: 404 (Resource Not Found)
        res.status(404).send('The building with the given ID does not exist.');
        return;
    }

    buildings = buildings.filter((keepBuilding) =>{
        return keepBuilding != building;
    });

    // Return building object for the ID provided in the request.
    res.status(200).send(building);
    return;
});

// go - Go Run Elevators in Queue
// Will goToFloor when "elevator.startQueue" is called.
// METHOD: GET
// RETURNS: building.actionQueue
router.get("/:id/go", (req, res) => {      
    let building = buildings.find(b => b.id === parseInt(req.params.id));    
    if(!building){
        // Return HTTP Code: 404 (Resource Not Found)
        res.status(404).send('The building with the given ID does not exist.');
        return;
    }
    if(building.actionQueue.length > 0){
        let queueProcessed = building.actionQueue;
        building.startActionQueue();
        // Return all of the buildings elevators when "go" runs. // *Change
        res.status(200).send(queueProcessed);
        return;
    }else{
        // No actions in queue for provided building id
        res.status(200).send(`No actions in queue for building id: ${building.id}`);
        return;
    }
});

// view-queue - Return Elevators in Queue
// Will goToFloor when "elevator.startQueue" is called.
// METHOD: GET
// RETURNS: building.actionQueue
router.get("/:id/view-queue", (req, res) => {      
    let building = buildings.find(b => b.id === parseInt(req.params.id));
    if(!building){
        // Return HTTP Code: 404 (Resource Not Found)
        res.status(404).send('The building with the given ID does not exist.');
        return;
    }
    console.log(`Action: 'viewQueue' view queue for Building id: ${building.id}`);
    // Return all of the buildings elevators when "go" runs. // *Change
    res.status(200).send(building.actionQueue);
    return;
});

module.exports = router;