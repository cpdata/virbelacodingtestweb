const express = require('express');
const router = express.Router();
const swaggerDoc = require('../swagger.json');
router.get("/", (req, res) => {
    res.status(200).send(swaggerDoc);
    return;
});

module.exports = router;