# Virbela Code Testing Web Assesment 

- **Performed by** : Charles P. Cross 
- <i>Senior NodeJS Developer, Design Architect, & Solutions Expert - **cpc7831@gmail.com** - **562-285-7993**</i>

----------------------------------------------------------------------------------------------------------------

# Table of Contents

- Project and what it does
- Setup
- Purpose & Intention with this project
- Design Implementation & Process
- Recomendations
- Installation Instuctions
- Links
	- DEMO URL
	- Postman Collecion Link
	- Swaggar Docs
	- Repository URL
- RESTful API Routes
- Examples

----------------------------------------------------------------------------------------------------------------

# Envrionment Setup
- a. OS - Linux Ubuntu 20 LTS
- b. IDE - Sublime / Visual Studio
- c. Server - Express
- d. Language - Node.js 
- e. Packages, Libraries, & Includes - Express
- f. Database / Persistent Storage - Global Object

----------------------------------------------------------------------------------------------------------------

# Purpose & Intention with this project

- a. Purpose - Showcase Node.js competency by completeing Virbela Coding Challenge Exercise 2 
- b. Intention - To demonstrate my aptitude for the Senior Node.js Developer Role at Virbela.

----------------------------------------------------------------------------------------------------------------

# Design Implementation & Process

- a. Design Stradegy - Plan routes according to requirements and make modules for the logic and the routes.
- b. Design Implementation - Buildings have floors and elevators. There can me many buildings and each building can have many elevators.
- c. Execution of Processes
Get Buildings as a collection or elevators per buildings or a specic elevator.
Command and elevator to openDoor,CloseDoor or goToFloor and each action is added to a queue.
When the "go" route endpoint is called all the actions for that building id will be executed instantly.

----------------------------------------------------------------------------------------------------------------

# Recomendations for using this project

- a. Use the Swaggar Doc Link to Test REST API with Swagger docs => https://elevator.cpdatadesigns.com/docs or the Postman Collection Link to Test Project https://www.getpostman.com/collections/2064ed430c1049805a05
- b. {{URL}} in the postman collection uses the DEMO URL https://elevator.cpdatadesigns.com or you can change the {{URL}} variable for the collection.
- c. There are 4 buildings by default. Lookup an elevator and call an action on it ex.[open-door,close-door,go-to-floor] then run "go" for a building id ex. /buildings/4/go to process all the actions in the queue.
- d. To use the API use the paths from the [RESTful API Routes] section below.

----------------------------------------------------------------------------------------------------------------

# Installation Instuctions

<code>npm install --save-dev express</code>
<code>npm install --save-dev swagger-ui-express</code>
<code>npm install --save-dev jest</code>

To run:
<code>node Exercise2/index.js</code>

Server listens on http://localhost:3000

If your enviroment varable for $PORT is assigned then server will listen on that port number instead.


If you need to install node:
# Install Node
<code>sudo apt install node npx npm</code> # check
<code>npm install --save-dev express</code>
<code>npm install --save-dev swagger-ui-express</code>
<code>npm install --save-dev jest</code>
- <i>**!NOTE:** if vulnerability issues arise</i>
<code>npm audit fix</code>
<code>npm audit fix --force</code>
<code>node Exercise2/index.js</code>

# Run Unit Tests
<code>npm test</code>

----------------------------------------------------------------------------------------------------------------

# Links

# DEMO URL
https://elevator.cpdatadesigns.com

# Postman Collecion Link - JSON link
https://www.getpostman.com/collections/2064ed430c1049805a05

# Swaggar Docs
LIVE URL: https://elevator.cpdatadesigns.com/docs
LOCAL URL: http://localhost:3000/docs
YAML: /swagger.yml

# Repository URL
https://gitlab.com/cpdata/virbelacodingtestweb

----------------------------------------------------------------------------------------------------------------

# RESTful API Routes

- All of the availble endpoint routes and accepted methods are returned by the root URL. Below are all the REST API resource paths for this project.
<code>
	{
        "resources":[
            {"buildings":[
                    {"path":"/buildings","methods":["GET","POST"],"desc":"Get all building objects or add a new building."},
                    {"path":"/buildings/:id","params":["name","floors","elevators"],"methods":["GET","DELETE"],"desc":"Get a specific building object."}
                ]                        
            },
            {"elevators":[                    
                    {"path":"/buildings/:id/elevators","methods":["GET","POST"],"desc":"Get all elevators in a building or add a new elevator."},
                    {"path":"/buildings/:id/elevators/:elevatorId","methods":["GET","DELETE"],"desc":"Get a specific elevator object."}                            
                ]                        
            },
            {"actions":[
                    {"path":"/buildings/:id/elevators/:elevatorId/go-to-floor/:floorNum","methods":["GET"],"desc":"Queue an elevator to go to floor number."},
                    {"path":"/buildings/:id/elevators/:elevatorId/open-door","methods":["GET"],"desc":"Queue an elevator openDoor action."},
                    {"path":"/buildings/:id/elevators/:elevatorId/close-door","methods":["GET"],"desc":"Queue an elevator closeDoor action."},
                    {"path":"/buildings/:id/view-queue","methods":["GET"],"desc":"View all actions in buildings queue."},
                    {"path":"/buildings/:id/go","methods":["GET"],"desc":"Execute all elevator actions for all elevators in a specific building."}
                ]                        
            }
        ]
    }
</code>

Below are is the same as above but in more detail.

- (The site Root will return the data structure of the REST API so a client can use link routes dynamically. Versioning and maintenence would also much easier to manage this way.)

<b>root GET /</b>

# Buildings

- (Get List of ALL buildings and associated elevators)
<b>getBuildings GET /buildings</b>

- (Add a new building with floors and elevators)
<b>addBuilding POST /buildings PARAMS: [name,floors,elevators]</b>

- (Get details for a specific building and it's elevators using the buiding's id.)
<b>findBuilding GET /buildings/:id</b>

- (Delete a building using the buiding's id.)
<b>deleteBuilding DELETE /buildings/:id</b>

# Elevators

- (Get List of ALL elevators for a building using the building's id)
<b>getElevators GET /buildings/:id/elevators</b>

- (Add a new elevator to a specific building which can then be used)
<b>addElevator POST /buildings/:id/elevators</b>

- (Get status & details of a specific elevator by using the elevator's id and the building's id)
<b>findElevator GET /buildings/:id/elevators/:elevatorId</b>

- (Delete elevator by using the elevator's id and the building's id)
<b>deleteElevator DELETE /buildings/:id/elevators/:elevatorId</b>

# Elevator Commands

- (Add openDoor action to an elevators Queue)
<b>openDoor GET /buildings/:id/elevators/:elevatorId/open-door</b>

- (Add closeDoor action to an elevators Queue)
<b>closeDoor GET /buildings/:id/elevators/:elevatorId/close-door</b>

- (Add goToFloor action to an elevators Queue)
<b>goToFloor GET /buildings/:id/elevators/:elevatorId/go-to-floor/:floorNum</b>

# Building Queue Commands
- ( View Event Action Queue for a Building )
<b>viewQueue GET /buildings/:id/view-queue</b>

- (Execute Queue to end immediately)
<b>go GET /buildings/:id/go</b>

---------------------------------------------------------------------------------------------------------------

# Examples 

- Create Building - (Add a new building)
<b>POST</b> http://localhost:3000/buildings?name=Freedom+Tower&floors=94&elevators=5

<b>POST</b> https://elevator.cpdatadesigns.com/buildings?name=Freedom+Tower&floors=94&elevators=5

Returns building with id.

- Create Elevator (Use id to create elevator)
<b>POST</b> http://localhost:3000/buildings/4/elevators 

<b>POST</b> https://elevator.cpdatadesigns.com/buildings/4/elevators 

Returns elevator with id.

- Queue Elevator Action goToFloor - (Use the building id and elevator id to add a goToFloor action to a buildings actions queue.)
<b>GET</b> http://localhost:3000/buildings/4/elevators/222/go-to-floor/12

<b>GET</b> https://elevator.cpdatadesigns.com/buildings/4/elevators/222/go-to-floor/12

Returns all actions in the current queue for that building.

- Process Queue - (You can add other actions to  queue such as openDoor or closeDoor then run "go" to execute & log all actions instantly)

<b>GET</b> http://localhost:3000/buildings/4/go

<b>GET</b> https://elevator.cpdatadesigns.com/buildings/4/go

<b>Run Unit Test</b>

<code> npm test </code>

*Note: Currently only tests for the Elevator module.
---------------------------------------------------------------------------------------------------------------

# Exercise Questions

1. How can your implementation be optimized?
- A: In a production environment it would be optimal if the building and elevator state data was stored in an AWS DynamoDB for easy scalable storage. Utilizing AWS's SQS (Simple Queue Service) would allow many servers/instances/workers to process the action queues for the buildings. To further optimize the production environment I would run the RESTful API on AWS lambda functions. By using those AWS services in conjuction with AWS's API gateway the implementation would be a No-Server or Serverless RESTful API which would be effecient for development, security, and maintainence. This implementation being a Serverless deployment would make it optimal to scale infinitly and linearly and reducing costs to only pay for the exact amount of compute resources utilized and remove the need for complex server administration while also being a more secure production deployment solution.

How I would optimize the implementation of the design of the code for this exersice? I would add PUT/UPDATE and DELETE routes for the Buildings and Elevators to expand the implemetned CREATE & READ actions to have complete CRUD for the resource collections. I would implement a proper database such as mongoDB or AWS's DynamoDB for storing the state information of the elevators and buildings.

Passenger logic could be added with a passenger REST routes and js objects to represent them. Passenget objects could play into the dynamics of how elevators in a system actually get used which would create a more complete system to model elevators in a building. They would be factored into capacity limits of both the buildings and elevators which would add more realistic complexity to the triggering of actions and additional stops for overcapacity stops. Knowing average capacity for elevators for times of day and on which floors would enable more intellegent elevator wait positioning.

For the logic of the elevators I would add values for capacity and speed/rate of floors per minute/sec. Then add delays for opening and closing the doors. The sequence of actions could be spread out and delayed to simulate a real elevator. Furthermore adding "up" and "down" values to the goToFloor api call to simulate how real elevators work. By having that additional logic I would be able to make a true to world algorithm for how the elevator sequence between floors picks up passengers from each floor. 

For the logic of the buildings I would add floor capacity limits as well as current and average capacity. This would allow more intellgent goToWaitPosition logic to position the elevator in positions that are most likely to reduce the distance needed to travel to stop at the next floor for passenger pickup.

To further optimize or enhance this project a client side front-end to go with it would help visualize the actions of the RESTful api. 

I think with all of the above optimizations this project could be a full elevator simulator. The algorithm to optimize the sequence between the floors could be set with controlable parameters to make a game out with the aim being the elevator doorman that is trying to move as many passengers in the least amount of time/distance. Basically being a elevator doorman time trial game. 

Security - Add apikey and/or auth credentials

2. How much time did you spend on your implementation?
- A: 
- Plan & Design: 30 Min
- Phase 1 Setup: 30 Min 
- Setup DEMO on web facing Server w/ SSL: 30 Min
- Write Code: 
   - RESTful API Phase 2: 4 hrs    
   - Queue Phase 4: 1 hr
- Swagger Documentation 30 Min
- Create Postman Collection & Testing: 1 hr

Total: 8 hours

3. What was most challenging for you?
- A: Showcasing my expertise as much as possible within a small scope using the requirements of this exercise.
