/*
Filename: elevator.test.js
Author: Charles P. Cross
Description: Elevator Class Test File
*/

global.lastElevatorId = 0;
global.lastBuildingId = 0;
global.lastQueueEntryId = 0;
global.validationError = "";

const Elevator = require('../modules/elevator');

function createElevator(){
	let elevator = new Elevator({	            
	            buildingId: 1,	            
	            availableFloors: 12	            
	            });

	return elevator;
}

describe('Elevator', () =>{
	let elevator = createElevator();	
	it('should have the proper defaults defined on object initiation.', () => {		
		expect(elevator.id).toBeGreaterThan(0);
		expect(elevator.buildingId).toBe(1);
	    expect(elevator.status).toBe("Waiting");
		expect(elevator.currentFloor).toBe(1);
		expect(elevator.availableFloors).toBe(12);
		expect(elevator.doorOpen).toBe(false);
		expect(elevator.lastDirection).toBe("down");	
    });

    it('should change doorOpen state to true when openDoor() is called.', () => {		
		elevator.openDoor();
		expect(elevator.doorOpen).toBe(true);
    });

    it('should change doorOpen state to false when closeDoor() is called.', () => {		
		elevator.closeDoor();
		expect(elevator.doorOpen).toBe(false);
    });

    it('stopQueue should contain the FloorNum sent by goToFloor as the most recent value in array.', () => {		
		elevator.goToFloor(3);
		expect(elevator.stopQueue).toBe(3);
		expect(elevator.stopQueue).toEqual(expect.arrayContaining([3]));
    });

});
